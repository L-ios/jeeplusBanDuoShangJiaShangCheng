package com.stylefeng.guns.core.beetl;

import java.util.List;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.shop.service.*;
import com.stylefeng.guns.persistence.shop.model.TGoods;
import com.stylefeng.guns.persistence.shop.model.TGoodsClass;
import org.springframework.stereotype.Component;


@Component
public class ProductClassFunctions {
	private ITOrderService orderService = SpringContextHolder.getBean(ITOrderService.class);
	private ITGoodsClassService goodsClassService = SpringContextHolder.getBean(ITGoodsClassService.class);
	private ITCartService CartService = SpringContextHolder.getBean(ITCartService.class);
	private ITGoodsService productService = SpringContextHolder.getBean(ITGoodsService.class);
	private ITArticleService articleService = SpringContextHolder.getBean(ITArticleService.class);
	private ITStoreService itStoreService = SpringContextHolder.getBean(ITStoreService.class);
	private ITFloorService itFloorService = SpringContextHolder.getBean(ITFloorService.class);


//	public List<ShopType> getShopTypeListByPid(Long pid){
//		ShopType gc=new ShopType();
//		gc.setParentId(pid);
//		return ShopTypeService.select(gc);
//	}
	/**
	 * 根据商品类型查询商品
	 * @param tid
	 * @return
	 */
	public List<TGoods> getProductListByTypeid(Long tid){
		TGoods goods=new TGoods();
		goods.setClassid(tid);
		EntityWrapper<TGoods> ew = new EntityWrapper<>(goods);
		return productService.selectList(ew);
	}
	/**
	 * 查询楼层商品
	 * @param tid
	 * @return
	 */
	public List<TGoods> getProductByFloorid(Long tid){
		return productService.selectProductByFloor(tid);
	}
	/**
	 * 根据订单查询商品
	 * @param tid
	 * @return
	 */
//	public List<Product> getProductListByOrderid(Long tid){
//		Product goods=new Product();
//		
//		return ProductService.selectPage(1, 6, goods).getList();
//	}
	/**
	 * 得到购物车数量
	 * @return
	 */
	public int selectOwnCartCount(Long id){
		return CartService.selectOwnCartCount(id);
	}
}
