package com.stylefeng.guns.persistence.shop.dao;

import com.stylefeng.guns.persistence.shop.model.TComment;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface TCommentMapper extends BaseMapper<TComment> {

}