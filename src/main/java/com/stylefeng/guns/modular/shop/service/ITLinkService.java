package com.stylefeng.guns.modular.shop.service;

import com.stylefeng.guns.persistence.shop.model.TLink;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface ITLinkService extends IService<TLink> {
	
}
