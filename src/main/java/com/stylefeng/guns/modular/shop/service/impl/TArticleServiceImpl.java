package com.stylefeng.guns.modular.shop.service.impl;

import com.stylefeng.guns.persistence.shop.model.TArticle;
import com.stylefeng.guns.persistence.shop.dao.TArticleMapper;
import com.stylefeng.guns.modular.shop.service.ITArticleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TArticleServiceImpl extends ServiceImpl<TArticleMapper, TArticle> implements ITArticleService {
	
}
