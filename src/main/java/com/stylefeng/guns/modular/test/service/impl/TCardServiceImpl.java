package com.stylefeng.guns.modular.test.service.impl;

import com.stylefeng.guns.persistence.test.model.TCard;
import com.stylefeng.guns.persistence.test.dao.TCardMapper;
import com.stylefeng.guns.modular.test.service.ITCardService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zscat
 * @since 2017-05-22
 */
@Service
public class TCardServiceImpl extends ServiceImpl<TCardMapper, TCard> implements ITCardService {
	
}
