package com.stylefeng.web.controller;


import com.stylefeng.web.wx.Constant;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 微信回调处理
 *
 * @author : Hui.Wang [huzi.wh@gmail.com]
 * @version : 1.0
 * @created on  : 2017/6/28  下午5:21
 */
@Controller
@RequestMapping(Constant.WX_CALLBACK_URI)
public class WxCallbackController extends BaseController {

}
